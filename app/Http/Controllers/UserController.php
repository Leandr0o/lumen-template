<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function __construct() {}

    public function login(Request $request) {
        try{
            $user = User::where('user',$request->user)->firstOrFail();
            if (Hash::check($request->password, $user->password)) {
                return $user;
            }
            abort(403, 'Wrong password');
        }catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            abort(403, 'Wrong user');
        }
    }
}
