<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        $adminUser = User::where('user', 'Admin')->first();
        if (!$adminUser) {
            $adminUser = new User();
            $adminUser->user = "Admin";
            $adminUser->email = "leandro.kondratzky@gmail.com";
            $adminUser->password = Hash::make("123456");
            $adminUser->save();
        }
    }
}
